import React from 'react';

const Noticia = ({ noticia }) => {

    // Extraemos los datos
    const { urlToImage, url, description, title, source } = noticia;

    const imagen = (urlToImage) ?
        <div className="card-image">
            <img
                src={urlToImage}
                alt={title}
            />
        </div>
        : null
    return (
        <div className="col s12 m6 l4">
            <div className="card">
                {imagen}
                <div className="card-content">
                    <h4>{title}</h4>
                    <p>{description}</p>
                </div>
                <div className="card-action">
                    <a 
                    href={url}
                    target="_blank" 
                    rel="noopener noreferrer"
                    className="waves-effect waves-ligth btn"
                    >Ver Noticia Completa </a><br/>
                    <small>Noticia de <i>{source.name}</i></small>
                </div>
            </div>
        </div>

    );
}

export default Noticia;