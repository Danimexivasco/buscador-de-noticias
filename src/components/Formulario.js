import React from 'react';
import styles from './Formulario.module.css';
import useSelect from '../hooks/useSelect';

const Formulario = ({guardarCategoria}) => {

    const opciones = [
        {value:"general", label:"General"},
        {value:"technology", label:"Tecnología"},
        {value:"science", label:"Ciencia"},
        {value:"business", label:"Negocios"},
        {value:"sports", label:"Deportes"},
        {value:"health", label:"Salud"},
        {value:"entertainment", label:"Entretenimiento"},
    ]
    
    // Utilizamos el custom Hook
    const [categoria, SelectNoticias] = useSelect('general', opciones);

    // Pasamos la categoria a APP
    const obtenerCategoria = e =>{
        e.preventDefault();

        guardarCategoria(categoria)
    }

    return ( 
        <div className={`${styles.buscador} row`}>
            <div className="col s12 m8 offset-m2">
                <form
                    onSubmit= {obtenerCategoria}
                >
                    <h2 className={styles.heading}>Selecciona una Categoría</h2>
                    <SelectNoticias />
                    <div className="input-field col s12">
                        <input
                            type="submit"
                            className={`${styles['btn-block']} btn-large amber darken-2 `}
                            value="Buscar"
                        />
                    </div>
                </form>
            </div>
        </div>
     );
}
 
export default Formulario;