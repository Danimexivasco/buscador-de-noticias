import React, {useState} from 'react';

const useSelect = (stateInicial, opciones) =>{

    // State del custom Hook
    const [state, setState] = useState(stateInicial)

    const SeleccionarCategoria = () =>(
        <select
            className="browser-default"
            value={state}
            onChange={e => setState(e.target.value)}
        >
            {opciones.map(opcion=>(
                <option key={opcion.value} value={opcion.value}>{opcion.label}</option>
            ))}
            {/* <option value=""> --Seleccione--</option> */}
        </select>

    )

    return [state, SeleccionarCategoria];
}

export default useSelect;